var isFirefox = typeof browser != "undefined";
var isChrome = !isFirefox;
var browser_api = isChrome ? chrome : browser;

/*----------Steam header stuff----------*/
function modifyHeaders(headers, to_set) {
	for (var header in headers) {
		var index = to_set.findIndex((header2) => {
			return headers[header].name.toLowerCase() == header2.name.toLowerCase();
		});
		if (index != -1) {
			if (to_set[index].value === null)
				headers.splice(header, 1);
			else
				headers[header].value = to_set[index].value
			to_set.splice(index, 1);
		}
	}
	for (var header in to_set) {
		if (to_set[header].value !== null)
			headers.push(to_set[header]);
	}
}

// Check if we should even modify this request
function isValidRequest(e) {
	// Check if the request has the necessary information we need
	if ((!e.originUrl && !e.initiator) || !e.url)
		return false;

	// Verify that the request came from a trusted page
	var source = new URL(e.originUrl || e.initiator);
	return source.hostname.endsWith("cathook.club");
}

function onHeaders(e) {
	if (!isValidRequest(e) || !e.responseHeaders)
		return;

	var source = new URL(e.originUrl || e.initiator);
	var target = new URL(e.url);
	switch (target.hostname) {
		case "mail.google.com":
			modifyHeaders(e.responseHeaders, [{
				name: "Access-Control-Allow-Origin",
				value: source.origin
			}, {
				name: "Access-Control-Allow-Credentials",
				value: "true"
			}, {
				name: "Access-Control-Allow-Headers",
				value: "authorization"
			}]);
			break;

		case "mail-attachment.googleusercontent.com":
			modifyHeaders(e.responseHeaders, [{
				name: "Access-Control-Allow-Origin",
				value: "null"
			}, {
				name: "Access-Control-Allow-Credentials",
				value: "true"
			}]);
			break;
		case "store.steampowered.com":
			modifyHeaders(e.responseHeaders, [{
				name: "Access-Control-Allow-Origin",
				value: source.origin
			}, {
				name: "Access-Control-Allow-Credentials",
				value: "true"
			}, {
				name: "X-Frame-Options",
				value: null
			}]);
			break;

		default:
			break;
	}

	return {
		responseHeaders: e.responseHeaders
	};
}

function onAuth(e) {
	if (!isValidRequest(e))
		return;
	if (e.url == "https://mail.google.com/mail/feed/atom") {
		return { cancel: true };
	}
}

// Strip/modify headers that stop us from interacting with the steam page
browser_api.webRequest.onHeadersReceived.addListener(onHeaders, {
	// We don't have access to all of these by default, but we may get access in the future thru setupPermissions
	urls: ["https://store.steampowered.com/*", "https://mail.google.com/mail/*", "https://mail-attachment.googleusercontent.com/*"]
}, isFirefox ? ["blocking", "responseHeaders"] : ["blocking", "responseHeaders", "extraHeaders"]);

// Make sure the user doesn't get a stupid auth popup that doesn't work
if (!isFirefox)
	browser_api.webRequest.onAuthRequired.addListener(onAuth, {
		urls: ["https://mail.google.com/mail/*"]
	}, ["blocking"]);
/*----------Steam header stuff end----------*/

const gmailpermissions = {
	origins: ["https://mail.google.com/mail/*", "https://mail-attachment.googleusercontent.com/*"]
};

/*----------Addon communication----------*/
function sendReply(port, request, data) {
	port.postMessage({ id: request.id, data: data });
}

var current_native_captcha = null;

// Handle long-term comms with accgen content scripts
browser_api.runtime.onConnect.addListener(function (port) {
	port.onMessage.addListener(async function (request) {
		switch (request.data.task) {
			case "setupGmail":
				sendReply(port, request, await setupPermissions());
				break;
			case "version":
				// Tell the website about our version, may be important for backwards compat in the future
				sendReply(port, request, { version: browser_api.runtime.getManifest().version, apiversion: 6 });
				break;
			case "nativeCaptcha":
				current_native_captcha = {
					port, request
				}
				chrome.windows.create({
					url: "https://store.steampowered.com/join/#accgen",
					type: "panel"
				}, function (window) {
					if (!window) {
						sendReply(current_native_captcha.port, current_native_captcha.request, JSON.stringify({ error: "Failed to open new window. Please disable any popup blockers." }));
						current_native_captcha = null;
					}
					else
						current_native_captcha.window = window.id;
				});
			default:
				break;
		}
		return true;
	});
});

browser_api.windows.onRemoved.addListener((id) => {
	if (current_native_captcha && current_native_captcha.window == id) {
		sendReply(current_native_captcha.port, current_native_captcha.request, JSON.stringify({ error: "Window was closed before the captcha was solved." }));
		current_native_captcha = null;
	}
});

browser_api.runtime.onMessage.addListener(
	(data, sender, sendResponse) => {
		switch (data.task) {
			case "nativeCaptcha":
				if (current_native_captcha)
					sendReply(current_native_captcha.port, current_native_captcha.request, data.data);
				current_native_captcha = null;
				break;
			case "disableOtherExtensions":
				if (!isChrome)
					return;
				// We have detected that someone is messing with our window
				// If the developer of the "Project Centurion AG" extension is reading this,
				// please stop messing with join pages not created by your extension.
				// This code will stop triggering automatically.
				chrome.permissions.request({ permissions: ["management"] }, function (granted) {
					// Check if user approved the permissions
					if (!granted)
						return sendResponse(true);
					chrome.management.get("gfmlilhophdchbdmihgdiphkkeeepecj", (extension) => {
						if (extension && extension.enabled)
							chrome.management.setEnabled("gfmlilhophdchbdmihgdiphkkeeepecj", false, sendResponse.bind(undefined, true));
					});
				});
				return true;
			default:
				break;
		}
	}
);
/*----------Addon communication end----------*/

/*----------Gmail support----------*/
function setupPermissions() {
	return new Promise(async (resolve) => {
		if (isFirefox && await browser_api.permissions.contains(gmailpermissions))
			resolve({ success: true });
		else if (isChrome)
			browser_api.permissions.request(gmailpermissions, function (granted) {
				// Check if user approved the permissions
				if (granted)
					resolve({ success: true });
				else {
					resolve({ success: false, reason: "Addon permissions were denied." });
				}
			});
		else {
			let createData = {
				type: "detached_panel",
				url: "permissions.html",
				width: 250,
				height: 100
			};
			browser.windows.create(createData).then(function (window) {
				browser.windows.onRemoved.addListener(async (windowId) => {
					if (window.id == windowId)
						if (!await browser_api.permissions.contains(gmailpermissions)) {
							resolve({ success: false, reason: "Addon permissions were denied." });
						}
						else
							resolve({ success: true });
				});
			});
		}
	})
}
/*----------Gmail support end----------*/